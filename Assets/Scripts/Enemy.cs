using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int maxHealth = 100;
    public float currentHealth;
    public Animator animator;
    Transform healthBar;
    //public float defenseStat = 1.0f;

    public float timeSinceLastHit = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        animator = GetComponent<Animator>();
        // healthBar = transform.GetChild()
        // healthBar.SetActive(false);
        healthBar = gameObject.transform.Find("HealthBar");
        healthBar.gameObject.SetActive(false);
    }

    void Update(){
        timeSinceLastHit += Time.deltaTime;
        hideHealthBar();
    }

    public void TakeDamage(int damage){
        
        timeSinceLastHit = 0.0f;
        healthBar.gameObject.SetActive(true);

        currentHealth -= damage;
        animator.SetTrigger("Hurt");

        if(currentHealth <= 0){
            Die();
        }
    }

    void Die(){
        
        animator.SetBool("isDead", true);
        
        //delay health bar deactivate
        //healthBar.gameObject.SetActive(false);

        GetComponent<Collider>().enabled = false;
        this.enabled = false;

        //change rigidbody to kinematic upon death
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        rigidbody.isKinematic = true;
        
        //destroy gameobject after 2 seconds
        Destroy(gameObject, 2);
    }

    void hideHealthBar(){
        if (timeSinceLastHit > 2.0f){
            healthBar.gameObject.SetActive(false);
        }
    }
}

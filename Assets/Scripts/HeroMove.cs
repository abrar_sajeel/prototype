using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class HeroMove : MonoBehaviour
{
    public Rigidbody heroRigidbody;
    [SerializeField] public float moveSpeed;
    [SerializeField] public float jumpForce = 5f;
    [SerializeField] float airSpeed;
    private AnimationHandler animations;
    public bool isRunning;
    public bool isGrounded;
    //bool airMove;
    bool playerHasXMovement;
    bool playerHasZMovement;
    SpriteRenderer sprite;
    IHero hero;
    bool isJumping;
    
    
    

    private void Awake(){
        
        heroRigidbody = GetComponent<Rigidbody>();
        animations = GetComponent<AnimationHandler>();
        sprite = GetComponent<SpriteRenderer>();
        
        hero = new Knight();
        hero.InitializeHero();
        
        moveSpeed = hero.heroSpeed;
       
    }

    private void Update(){
        
        RestrictXMovementToCamera();
        
    }

    private void FixedUpdate(){
                
        //check if player is running to play animation
        checkRunning();
       
    }
    
   
    public void Move(Vector2 inputVector){
        if (isGrounded){
            heroRigidbody.velocity = new Vector3(inputVector.x * moveSpeed, heroRigidbody.velocity.y, inputVector.y * moveSpeed);
        } else if (!isGrounded) {
            heroRigidbody.velocity = new Vector3(inputVector.x * airSpeed, heroRigidbody.velocity.y, inputVector.y * airSpeed);
        }
    }

    public void Jump(){
        
        //jump while player is stationary
       if(isGrounded && !isRunning){
            heroRigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            airSpeed = hero.heroSpeed / 10;
            
        //jump while player is running   
       } else if (isGrounded && isRunning){
            heroRigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            airSpeed = hero.heroSpeed / 3;
        
       }    
       isJumping = true;
       //Play jump animation
       animations.SetJump(isJumping);
    }


    public void FlipSprite(Vector2 direction){
        
        if  (direction.x > 0){
            sprite.flipX = false;
        }
        else if (direction.x < 0){
            sprite.flipX = true;
        }
    }

    void checkRunning(){
        //check if player is moving on x or z axis
        playerHasXMovement = Mathf.Abs(heroRigidbody.velocity.x) > Mathf.Epsilon;
        playerHasZMovement = Mathf.Abs(heroRigidbody.velocity.z) > Mathf.Epsilon;

        isRunning = playerHasXMovement || playerHasZMovement;

        //play running animation
        animations.SetRunning(isRunning);
        
    }

    void RestrictXMovementToCamera(){
        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
        pos.x = Mathf.Clamp01(pos.x);
        transform.position = Camera.main.ViewportToWorldPoint(pos);
    }

    void OnCollisionEnter(Collision collision){
        if(collision.gameObject.name == "Ground"){
            isGrounded = true;
            isJumping = false;
            animations.SetJump(isJumping);
            moveSpeed = hero.heroSpeed;
        }
    }

     void OnCollisionExit(Collision collision){
        if(collision.gameObject.name == "Ground"){
            isGrounded = false;
           
        }
    }

}

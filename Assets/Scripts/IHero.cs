using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHero
{
    
    void InitializeHero();
    float heroSpeed{get; set;}
    
    void SpecialAttack();
   
}

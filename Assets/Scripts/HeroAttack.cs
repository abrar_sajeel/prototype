using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroAttack : MonoBehaviour
{
    
    private AnimationHandler animations;
    private HeroMove movement;
    public Transform attackPoint;
    public LayerMask enemyLayers;
    SpriteRenderer sprite;
    public int attackDamage = 40;
    private int currentAttack = 0;
    public float attackRange = 0.5f;
    public float timeSinceAttack = 0.0f;
    public float timeSinceCombo = 0.0f;
    IHero hero;
    
    private void Awake(){
        hero = new Knight();
        hero.InitializeHero();
       
        animations = GetComponent<AnimationHandler>();
        movement = GetComponent<HeroMove>();
        sprite = GetComponent<SpriteRenderer>();

        

        
        
    }
    
    // Start is called before the first frame update
    void Start()
    {
        //TODO fetch variables for each specific charachter
        // if (gameObject.name == "HeroKnight"){
        //     Debug.Log("This is hero knight");
           
        // }
    }

    // Update is called once per frame
    void Update()
    {
        SetTimers();
        ResetAttackState();
        ResetMoveSpeed();

    }

   
    //includes 3 hit combo
    public void GroundAttack()
    {
        if (timeSinceAttack > 0.15f && timeSinceCombo > 1.0f){
            
            if (currentAttack == 2){
                timeSinceCombo = 0.0f;
            }
            
            //reduce movement speed while attacking
            movement.moveSpeed = 0.25f;
            currentAttack++;
            
            if(currentAttack > 3){
               
                currentAttack = 1;
               
            } 

            
            animations.TriggerAttack(currentAttack);

            FlipAttackPoint();
           
            
            Collider[] hitEnemies = Physics.OverlapSphere(attackPoint.position, attackRange, enemyLayers);
            foreach(Collider enemy in hitEnemies){
                enemy.GetComponent<Enemy>().TakeDamage(attackDamage);
            }

            timeSinceAttack = 0.0f;
        }
    }
    
   void SetTimers(){
        timeSinceAttack += Time.deltaTime;
        timeSinceCombo += Time.deltaTime;
   }

   void ResetAttackState(){
        
        if (timeSinceAttack > 0.5f){
            currentAttack = 0;
            
        }

       
   }

   void ResetMoveSpeed(){
        if (timeSinceAttack > 0.40f){
            movement.moveSpeed = hero.heroSpeed;
        }
   }

   void FlipAttackPoint(){

        if (!sprite.flipX){
            attackPoint.position = new Vector3(transform.position.x + 1f, transform.position.y + 1f, transform.position.z);
        } else if (sprite.flipX){
            attackPoint.position = new Vector3(transform.position.x - 1f, transform.position.y + 1f, transform.position.z);
        }
   }

    void OnDrawGizmosSelected(){
        if (attackPoint == null){
            return;
        }
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }

  
}

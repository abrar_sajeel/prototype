using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputHandler : MonoBehaviour
{
    private PlayerInputActions playerInputActions;
    private HeroMove movement;
    private HeroAttack attack;
    private Vector2 moveInput;
    
    void Awake(){

        playerInputActions = new PlayerInputActions();
        movement = GetComponent<HeroMove>();
        attack = GetComponent<HeroAttack>();

    }

    // Update is called once per frame
    void Update(){
        
        HandleJump();
        HandleAttack();
        HandleMovement();

    }

    void FixedUpdate(){
       
       
       
    }

    void HandleMovement(){

        moveInput = playerInputActions.Player.Movement.ReadValue<Vector2>();
        movement.Move(moveInput);
        movement.FlipSprite(moveInput);

    }

    void HandleJump(){

        if (playerInputActions.Player.Jump.triggered){
            movement.Jump();

        }
    }

    void HandleAttack(){

        if (playerInputActions.Player.Attack.triggered){
            attack.GroundAttack();
        }
       
    }

    void OnEnable(){

        playerInputActions.Player.Enable();

    }

    void OnDisable(){

        playerInputActions.Player.Disable();
        
    }

}

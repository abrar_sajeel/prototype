using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knight : IHero
{
    
    public float heroSpeed{get; set;}
    

    public void InitializeHero(){
       
        heroSpeed = 10f;
        
    }

    public void SpecialAttack(){
        //Implement special attack logic
    }


}

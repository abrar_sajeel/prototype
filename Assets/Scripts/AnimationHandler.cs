using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    private Animator heroAnimator;
    // Start is called before the first frame update

    void Awake(){

        heroAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetRunning(bool isRunning){
        heroAnimator.SetBool("isRunning", isRunning);
    }

    public void TriggerAttack(int currentAttack){
         heroAnimator.SetTrigger("Attack" + currentAttack);
    }

    public void SetJump(bool jumping){
        heroAnimator.SetBool("isJumping", jumping);
    }

    // public void SetGrounded(bool isGrounded){

    // }
}

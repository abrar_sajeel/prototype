using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    Vector3 localScale;
    Enemy enemy;
    // Start is called before the first frame update
    void Start()
    {
        localScale = transform.localScale;
        
        enemy = gameObject.GetComponentInParent<Enemy>();
    }

    // Update is called once per frame
    void Update()
    {
       reduceHealth();
    }

    void reduceHealth(){
        
        
        if(enemy.currentHealth > 0.0f){
            //localScale.x = (enemy.currentHealth / 100f);
            localScale.x = (enemy.currentHealth / enemy.maxHealth);
        }else{
            localScale.x = 0f;
        }
        transform.localScale = localScale;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputHandler : MonoBehaviour
{

    public Vector2 RawMovementInput {get; private set;}
    public int NormInputX {get; private set;}
    public int NormInputZ {get; private set;}

    public void OnMoveInput(InputAction.CallbackContext context){
       
        RawMovementInput = context.ReadValue<Vector2>();
        //Dont allow analog input between 0 and 1
        NormInputX = (int)(RawMovementInput * Vector2.right).normalized.x;
        NormInputZ = (int)(RawMovementInput * Vector2.up).normalized.y;
    }

    public void OnJumpInput(InputAction.CallbackContext context){
        
    }

}

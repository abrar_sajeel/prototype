using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateMachine
{   
    public PlayerState CurrentState {get; private set;} //any script can get currentstate but only this script sets it

    public void Initialize(PlayerState startingState){
        CurrentState = startingState;
        CurrentState.Enter();
    }

    public void ChangeState(PlayerState newState){
        CurrentState.Exit();
        CurrentState = newState;
        CurrentState.Enter();
    }
}

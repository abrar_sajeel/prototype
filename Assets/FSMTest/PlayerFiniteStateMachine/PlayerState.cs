using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState{

    protected Player player;
    protected PlayerStateMachine stateMachine;
    protected PlayerData playerData;
    protected float startTime; //Reference to how long we've been in a specific state
    private string animBoolName; //reference to animation state
    
    ///constructor
    public PlayerState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animBoolName){
        this.player = player;
        this.stateMachine = stateMachine;
        this.playerData = playerData;
        this.animBoolName = animBoolName;
    }

    //virtual methods can be overriden in child classes
    //Enter called when entering a state
    public virtual void Enter(){
        DoChecks();
        player.Anim.SetBool(animBoolName, true);
        startTime = Time.time;
        Debug.Log("Current state: " + animBoolName);
        
    }
    //Exit called when leaving a state
    public virtual void Exit(){
        player.Anim.SetBool(animBoolName, false);
    }
    //LogicUpdate gets called every frame (Update)
    public virtual void LogicUpdate(){
        
    }
    //Physics Update gets called every FixedUpdate
    public virtual void PhysicsUpdate(){
        DoChecks();
    }
    //DoChecks called in physics update and enter, e.g look for ground etc
    public virtual void DoChecks(){
        
    }

}

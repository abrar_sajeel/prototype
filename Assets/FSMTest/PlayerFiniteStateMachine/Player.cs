using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region State Variables
    public PlayerStateMachine StateMachine {get; private set;}
    public PlayerIdleState IdleState {get; private set;}
    public PlayerMoveState MoveState {get; private set;}
    [SerializeField] private PlayerData playerData;
    #endregion

    #region Components
    public Rigidbody RB {get; private set;}
    public SpriteRenderer sprite {get; private set;}

    public Animator Anim {get; private set;}
    public PlayerInputHandler InputHandler {get; private set;}
    #endregion

    #region Other 
    private Vector3 workSpace;
    public Vector3 CurrentVelocity {get; private set;}
    public int FacingDirection {get; private set;}
    #endregion

    #region Unity Callback Functions
    private void Awake(){
        StateMachine = new PlayerStateMachine();

        IdleState = new PlayerIdleState(this, StateMachine, playerData, "idle");
        MoveState = new PlayerMoveState(this, StateMachine, playerData, "move");
    }

   
    private void Start(){

        Anim = GetComponent<Animator>();
        InputHandler = GetComponent<PlayerInputHandler>();
        RB = GetComponent<Rigidbody>();
        sprite = GetComponent<SpriteRenderer>();
        
        FacingDirection = 1;

        StateMachine.Initialize(IdleState);
    }

    private void Update(){
        CurrentVelocity = RB.velocity;
        StateMachine.CurrentState.LogicUpdate();
    }

    private void FixedUpdate(){
        StateMachine.CurrentState.PhysicsUpdate();
    }
    #endregion

    #region Set Functions
    public void SetVelocity(float velocityX, float velocityZ){
        workSpace.Set(velocityX, CurrentVelocity.y, velocityZ);
        RB.velocity = workSpace;
        CurrentVelocity = workSpace;
    }
    #endregion

    #region Check Functions
    public void CheckIfShouldFlip(int xInput){
        if (xInput != 0 && xInput != FacingDirection){
            Flip();
        }
    }
    #endregion
    
    #region Other Functions
    private void Flip(){
        FacingDirection *= -1;
        if (FacingDirection == 1){
            sprite.flipX = false;
        }else{
            sprite.flipX = true;
        }

    }
    #endregion
}
